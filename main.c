#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

struct _3dsx_header {
	uint8_t		magic[4];
	uint16_t	header_size;
	uint16_t	relocation_hdr_size;
	uint32_t	format_version;
	uint32_t	flags;
	uint32_t	code_seg_size;
	uint32_t	rodata_seg_size;
	uint32_t	data_seg_size;
	uint32_t	bss_seg_size;
} __attribute__((packed));

struct _3dsx_relocation_hdr {
	uint32_t	n_abs_relocations;
	uint32_t	n_rel_relocations;
} __attribute__((packed));

struct _3dsx_relocation {
	uint16_t	n_words_skip;
	uint16_t	n_words_patch;
} __attribute__((packed));

#define _3DSX_HEADER_SIZE 	(sizeof(struct _3dsx_header))
#define _3DSX_MAGIC_0		'3'
#define _3DSX_MAGIC_1		'D'
#define _3DSX_MAGIC_2		'S'
#define _3DSX_MAGIC_3		'X'

void usage();

int _3dsx_read_header(FILE *fp, struct _3dsx_header *hdr);
void _3dsx_print_header(struct _3dsx_header *hdr);
int _3dsx_header_is_valid(struct _3dsx_header *hdr);
void _3dsx_extract_segments(FILE *fp, struct _3dsx_header *hdr, char *basename);

int main(int argc, char *argv[])
{
	if (argc < 2) {
		usage();
		return -1;
	}
	
	FILE *fp;
	if ((fp = fopen(argv[1], "rb")) == NULL) {
		printf("Could not open file \"%s\"\n", argv[1]);
		return -1;
	}
	
	struct _3dsx_header header;
	if (!_3dsx_read_header(fp, &header)) {
		printf("Could not read the header.\n");
		fclose(fp);
		return -1;
	}
	
	_3dsx_print_header(&header);
	
	if (_3dsx_header_is_valid(&header)) {
		printf("Valid header\n");
	} else {
		printf("Not a valid header\n");
		fclose(fp);
		return -1;
	}
	
	_3dsx_extract_segments(fp, &header, argv[1]);
	
	fclose(fp);
	return 0;
}

static int extract_segment(FILE *fp, uint32_t size, char *out_name)
{
	void *buffer = malloc(size);
	size_t ret1 = fread(buffer, 1, size, fp);
	FILE *out_fp = fopen(out_name, "wb");
	size_t ret2 = fwrite(buffer, 1, size, out_fp);
	fclose(out_fp);
	free(buffer);
	return ((ret1 == size) && (ret2 == size));
}

void _3dsx_extract_segments(FILE *fp, struct _3dsx_header *hdr, char *basename)
{
	char name_str[FILENAME_MAX];
	
	fseek(fp, hdr->header_size + 3*sizeof(struct _3dsx_relocation_hdr), SEEK_SET);
	printf("Extracting code segment...");
	sprintf(name_str, "%s.code", basename);
	extract_segment(fp, hdr->code_seg_size, name_str);
	printf("done\n");

	printf("Extracting rodata segment...");
	sprintf(name_str, "%s.rodata", basename);
	extract_segment(fp, hdr->rodata_seg_size, name_str);
	printf("done\n");
	
	printf("Extracting data segment...");
	sprintf(name_str, "%s.data", basename);
	extract_segment(fp, hdr->data_seg_size, name_str);
	printf("done\n");
}

int _3dsx_read_header(FILE *fp, struct _3dsx_header *hdr)
{
	return (fread(hdr, 1, _3DSX_HEADER_SIZE, fp) == _3DSX_HEADER_SIZE);
}

void _3dsx_print_header(struct _3dsx_header *hdr)
{
	printf("Magic: 0x%08X (%c%c%c%c)\n", *(uint32_t *)hdr->magic,
		hdr->magic[0], hdr->magic[1], hdr->magic[2], hdr->magic[3]);
	printf("Header size: 0x%04X\n", hdr->header_size);
	printf("Relocation header size: 0x%04X\n", hdr->relocation_hdr_size);
	printf("Format version: 0x%08X\n", hdr->format_version);
	printf("Flags: 0x%04X\n", hdr->flags);
	printf("Code segment size: 0x%08X\n", hdr->code_seg_size);
	printf("Rodata segment size: 0x%08X\n", hdr->rodata_seg_size);
	printf("Data segment size: 0x%08X\n", hdr->data_seg_size);
	printf("BSS segment size: 0x%08X\n", hdr->bss_seg_size);
}

int _3dsx_header_is_valid(struct _3dsx_header *hdr)
{
	return	((hdr->magic[0] == _3DSX_MAGIC_0) &&
			(hdr->magic[1] == _3DSX_MAGIC_1) &&
			(hdr->magic[2] == _3DSX_MAGIC_2) &&
			(hdr->magic[3] == _3DSX_MAGIC_3));
}

void usage()
{
	printf("3dsx_reader by xerpi\n");
	printf("usage:\n\t3dsx_reader file[.3dsx]\n");
}
