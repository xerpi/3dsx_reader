TARGET = 3dsx_reader
OBJS   = main.o
CFLAGS = -Wall -Wno-strict-aliasing -O2

all: $(TARGET)

$(TARGET): $(OBJS)
	g++ $< -o $@
	
%.o: %.c
	g++ $(CFLAGS) -c $< -o $@

clean:
	@rm -rf $(TARGET) $(OBJS)
